Rails.application.routes.draw do
  #root 'home#index'
  devise_for :users, controllers: {registrations: "registrations"}

  resources :home
  get 'home/show'
  get 'home/index'
  #get 'welcome/index'
  get "send_mail", to: "welcome#index"
  #root 'welcome#index'
  


  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
