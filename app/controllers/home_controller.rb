class HomeController < ApplicationController
  def index

  	if user_signed_in?
  		@c = Candidate.all
  		render :index
  	else
  		redirect_to new_user_session_path
  	end
  end
  def show
  	@c  = Candidate.find(params[:id])
  end

  def destroy
    @c = Candidate.find(params[:id])
    @c.destroy

    redirect_to home_path
end





  end
